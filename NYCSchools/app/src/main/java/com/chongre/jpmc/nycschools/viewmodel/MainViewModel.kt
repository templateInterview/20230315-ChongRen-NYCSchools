package com.chongre.jpmc.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chongre.jpmc.nycschools.model.Repository
import com.chongre.jpmc.nycschools.model.Resource
import com.chongre.jpmc.nycschools.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(val repository: Repository) : ViewModel() {

    private val _schools = MutableLiveData<Resource<List<School>>>()
    val schools: LiveData<Resource<List<School>>> = _schools

    fun getNycSchools() {

        viewModelScope.launch(IO) {
            try {


                _schools.postValue(Resource.Loading)

                val response = repository.getNycSchools()

                if (!response.isSuccessful) {
                    _schools.postValue(Resource.Message("Failed to load data. Please retry."))
                    return@launch
                }

                val schoolList = response.body()

                if (schoolList == null || schoolList.isEmpty()) {
                    _schools.postValue(Resource.Message("Empty response from server"))
                    return@launch
                }

                _schools.postValue(Resource.Success(schoolList))
            } catch (e: Exception) {
                _schools.postValue(Resource.Message("Error is: $e"))
            }
        }
    }
}