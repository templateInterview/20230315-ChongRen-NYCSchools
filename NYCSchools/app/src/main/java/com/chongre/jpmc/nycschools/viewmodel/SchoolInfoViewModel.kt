package com.chongre.jpmc.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.chongre.jpmc.nycschools.model.Repository
import com.chongre.jpmc.nycschools.model.Resource
import com.chongre.jpmc.nycschools.model.SATData
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolInfoViewModel @Inject constructor(val repository: Repository) : ViewModel() {

    private val _satData = MutableLiveData<Resource<List<SATData>>>()
    val satData: LiveData<Resource<List<SATData>>> = _satData

    fun getSATData(dbn: String) {

        viewModelScope.launch(IO) {
            try {


                _satData.postValue(Resource.Loading)

                val response = repository.getSATData(dbn)

                if (!response.isSuccessful) {
                    _satData.postValue(Resource.Message("Failed to load data. Please retry."))
                    return@launch
                }

                val schoolList = response.body()

                if (schoolList == null) {
                    _satData.postValue(Resource.Message("Empty response from server"))
                    return@launch
                }

                _satData.postValue(Resource.Success(schoolList))
            } catch (e: Exception) {
                _satData.postValue(Resource.Message("Error is: $e"))
            }
        }
    }
}