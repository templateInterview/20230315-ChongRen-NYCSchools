package com.chongre.jpmc.nycschools.model

import androidx.lifecycle.MutableLiveData
import javax.inject.Inject

class Repository @Inject constructor(val apiService: ApiService) {

    suspend fun getNycSchools() = apiService.getNycSchools()

    suspend fun getSATData(dbn: String) = apiService.getSATScore(dbn)
}