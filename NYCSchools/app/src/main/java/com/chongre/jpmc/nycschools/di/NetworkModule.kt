package com.chongre.jpmc.nycschools.di

import android.content.SharedPreferences
import com.chongre.jpmc.nycschools.BuildConfig
import com.chongre.jpmc.nycschools.common.Constants
import com.chongre.jpmc.nycschools.model.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun providesClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().apply {
            if(BuildConfig.DEBUG) {
                level = HttpLoggingInterceptor.Level.BODY
            } else {
                level = HttpLoggingInterceptor.Level.NONE
            }
        }
        val client = OkHttpClient.Builder().apply {
            connectTimeout(60, TimeUnit.SECONDS)
            readTimeout(60, TimeUnit.SECONDS)
            writeTimeout(60, TimeUnit.SECONDS)
            addInterceptor(interceptor)
        }.build()
        return client
    }

    @Provides
    fun providesRetrofit(client: OkHttpClient): Retrofit {

        val r = Retrofit.Builder().apply {
            baseUrl(Constants.BASE_URL)
            addConverterFactory(GsonConverterFactory.create())
            client(client)
        }.build()

        return r
    }


    @Provides
    fun providesApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)



}