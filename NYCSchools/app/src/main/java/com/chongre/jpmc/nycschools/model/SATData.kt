package com.chongre.jpmc.nycschools.model

import com.google.gson.annotations.SerializedName

data class SATData(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("num_of_sat_test_takers")
    val testTakers: String,

    @SerializedName("sat_critical_reading_avg_score")
    val criticalReadingAvgScore: String,

    @SerializedName("sat_math_avg_score")
    val mathAvgScore: String,

    @SerializedName("sat_writing_avg_score")
    val writingAvgScore: String
)