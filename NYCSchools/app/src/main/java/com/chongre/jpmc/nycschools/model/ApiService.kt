package com.chongre.jpmc.nycschools.model

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("resource/s3k6-pzi2.json")
    suspend fun getNycSchools(): Response<List<School>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATScore(
        @Query("dbn") dbn: String
    ): Response<List<SATData>>

}