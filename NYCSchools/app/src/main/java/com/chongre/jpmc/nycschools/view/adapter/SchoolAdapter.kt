package com.chongre.jpmc.nycschools.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.chongre.jpmc.nycschools.databinding.ItemSchoolBinding
import com.chongre.jpmc.nycschools.model.School

class SchoolAdapter(val schools: List<School>):
Adapter<SchoolViewHolder>(){
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemSchoolBinding.inflate(inflater, parent, false)
        return SchoolViewHolder(binding)
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.bind(schools[position])

        holder.itemView.setOnClickListener {
            if(this::schoolSelected.isInitialized) {
                schoolSelected(schools[position])
            }
        }
    }

    override fun getItemCount() = schools.size

    private lateinit var schoolSelected: (School) -> Unit

    fun setOnSchoolSelectedListener(listener: (School) -> Unit) {
        schoolSelected = listener
    }
}