package com.chongre.jpmc.nycschools.view

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import com.chongre.jpmc.nycschools.R
import com.chongre.jpmc.nycschools.databinding.ActivitySchoolInfoBinding
import com.chongre.jpmc.nycschools.model.Resource
import com.chongre.jpmc.nycschools.model.School
import com.chongre.jpmc.nycschools.view.adapter.SchoolAdapter
import com.chongre.jpmc.nycschools.viewmodel.SchoolInfoViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.selects.select

@AndroidEntryPoint
class SchoolInfoActivity : BaseActivity() {
    lateinit var selectedSchool: School
    val viewModel: SchoolInfoViewModel by viewModels()
    lateinit var binding: ActivitySchoolInfoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySchoolInfoBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            intent.extras?.getParcelable("school", School::class.java)?.let {
                selectedSchool = it

            }
        } else {
            intent.extras?.getParcelable<School>("school")?.let {
                selectedSchool = it
            }
        }
        setupObservers()
        loadSchoolInfo()
        setupEvents()
    }

    private fun setupEvents() {
        binding.btnRetry.setOnClickListener {
            if(!isNetworkAvailable()) {
                showNoInternetMessage()
                return@setOnClickListener
            }
            if(this::selectedSchool.isInitialized) {
                viewModel.getSATData(selectedSchool.dbn)
            }
        }


    }

    private fun loadSchoolInfo() {
        if(this::selectedSchool.isInitialized) {

            binding.apply {
                tvSchoolName.text = selectedSchool.schoolName
                val lastIndex = selectedSchool.location.lastIndexOf("(")
                if(lastIndex!=-1) {
                    tvLocation.text = selectedSchool.location.substring(0, lastIndex-1)
                } else {
                    tvLocation.text = selectedSchool.location
                }

                tvPhoneNumber.text = selectedSchool.phoneNumber
                tvEmail.text = selectedSchool.schoolEmail
                if(selectedSchool.website.startsWith("http")) {
                    tvWebsite.text = selectedSchool.website
                } else {
                    tvWebsite.text = "https://${selectedSchool.website}"
                }
            }
            if(!isNetworkAvailable()) {
                showNoInternetMessage()
                return
            }
            viewModel.getSATData(selectedSchool.dbn)
        } else {
            Toast.makeText(baseContext, R.string.error_school_object_missing, Toast.LENGTH_SHORT).show()
        }
    }

    private fun setupObservers() {
        viewModel.satData.observe(this) {
            when(it) {
                is Resource.Loading -> {
                    binding.apply {
                        loadingGroup.visibility = View.VISIBLE
                        satGroup.visibility = View.GONE
                        retryGroup.visibility = View.GONE
                    }
                }
                is Resource.Message -> {
                    binding.apply {
                        loadingGroup.visibility = View.GONE
                        satGroup.visibility = View.GONE
                        retryGroup.visibility = View.VISIBLE
                        tvRetryMsg.text = it.msg
                    }
                }

                is Resource.Success -> {
                    binding.apply {
                        loadingGroup.visibility = View.GONE

                        retryGroup.visibility = View.GONE
                        if(it.data.isEmpty()) {
                            tvSatLabel.visibility = View.VISIBLE
                            tvSatLabel.text = getString(R.string.no_sat_scrore_details_found)
                        } else {
                            satGroup.visibility = View.VISIBLE
                            val satScore = it.data[0]
                            tvTestTakers.text = getString(R.string.number_of_test_takers, satScore.testTakers)
                            tvCriticalReadingAvgScore.text = getString(R.string.sat_critical_reading_avg_score, satScore.criticalReadingAvgScore)
                            tvMathAvgScore.text = getString(R.string.sat_math_avg_score, satScore.mathAvgScore)
                            tvWritingAvgScore.text = getString(R.string.sat_writing_avg_score, satScore.writingAvgScore)
                        }

                    }
                }
            }
        }
    }
}